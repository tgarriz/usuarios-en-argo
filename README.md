# Usuarios En Argo

Escenario para aplicar en un cluster multidiciplinario que permita a clientes conectar a una instancia de argocd con permisos totales solo a sus proyectos, sin poder modificar proyectos de otros equipos

## Instalar cliente argocd
```bash
curl -sSL -o argocd-linux-amd64 "https://github.com/argoproj/argo-cd/releases/download/$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")')/argocd-linux-amd64"
chmod +x argocd-linux-amd64
sudo mv argocd-linux-amd64 /usr/local/bin/argocd
```

## Exponer servicio y loguearse con el cli
```bash
kubectl port-forward svc/argocd-server -n argocd 8080:443
argocd login localhost:8080
```

## Editar el configmap argocd-cm y agregar user agregando la sig. linea

accounts.cartouser: apiKey,login

```yaml
apiVersion: v1
data:
  accounts.cartouser: apiKey,login
  admin.enabled: "true"
  application.instanceLabelKey: argocd.argoproj.io/instance
  exec.enabled: "true"
  exec.shells: bash,sh
  server.rbac.log.enforce.enable: "false"
  statusbadge.url: https://argo2121-desa.test.com/
  timeout.hard.reconciliation: 0s
  timeout.reconciliation: 180s
  url: https://argo-desa.test.arba.gov.ar
kind: ConfigMap
metadata:
  annotations:
    meta.helm.sh/release-name: argo-cd
    meta.helm.sh/release-namespace: argocd
  creationTimestamp: "2024-03-12T11:53:47Z"
  labels:
    app.kubernetes.io/component: server
    app.kubernetes.io/instance: argo-cd
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: argocd-cm
    app.kubernetes.io/part-of: argocd
    app.kubernetes.io/version: v2.10.7
    helm.sh/chart: argo-cd-6.7.14
  name: argocd-cm
  namespace: argocd
  resourceVersion: "74653532"
  uid: 0e757953-9bf1-430f-9055-3c9277e03c39
```
## Luego setear la passowrd para el user (cartouser en este caso)

```bash
argocd account update-password \
  --account cartouser \
  --current-password '' \
  --new-password nuevapass
```
y si queremos generar un token para interactuar via api

```bash
argocd account generate-token --account cartouser
```
## Asignar los persmisos en el configmap argocd-rbac-cm

```yaml
apiVersion: v1
data:
  policy.csv: |
    p, role:cartouser, applications, get, carto/*, allow
    p, role:cartouser, applications, create, carto/*, allow
    p, role:cartouser, applications, update, carto/*, allow
    p, role:cartouser, applications, delete, carto/*, allow
    p, role:cartouser, applications, sync, carto/*, allow
    p, role:cartouser, applications, override, carto/*, allow
    p, role:cartouser, logs, get, carto/*, allow
    p, role:cartouser, exec, create, carto/*, allow
    g, cartouser, role:cartouser
  policy.default: role:readonly
  policy.matchMode: glob
  scopes: '[groups]'
kind: ConfigMap
metadata:
  annotations:
    meta.helm.sh/release-name: argo-cd
    meta.helm.sh/release-namespace: argocd
  creationTimestamp: "2024-03-12T11:53:47Z"
  labels:
    app.kubernetes.io/component: server
    app.kubernetes.io/instance: argo-cd
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: argocd-rbac-cm
    app.kubernetes.io/part-of: argocd
    app.kubernetes.io/version: v2.10.7
    helm.sh/chart: argo-cd-6.7.14
  name: argocd-rbac-cm
  namespace: argocd
  resourceVersion: "74686642"
  uid: de7b28ce-0728-47a7-ac0d-af8c9dcc5551
```
Por ultimo hacemos un rollout restart del deploy argocd-server

```bash
kubectl -n argocd rollout restart deployment argocd-server
```

